# My ricing (dot files and dwm)
![my screenshot](./screenshot.png)

dwm is an extremely fast, small, and dynamic window manager for X. And this is my configuration of it.

## Features of my build
* Support for brightness and volume keys
* Support for AZERTY keyboards (for the tags)
* Uses SUPER key (A.K.A ~~Windows~~ key) instead of ALT key
* Uses only 3 tags, who needs more?
* Doesn't have the weird "Firefox in X tag" thingys
* Decent status bar with colored emojis
* Other fonts
* Gaps and shortcuts for controlling them (vanitygaps patch)
* Resize floating windows with keyboard (moveresize patch)
* Nice wallpaper and transparent terminal
* Collection of color schemes for terminal

## Requirements
This is the list of used software for this project (on Debian, but should be pretty similar on Arch)

I had to re-compile libxft with the bgra patch myself to make colored emojis work properly (on Debian, if you use Arch, just install libxft-bgra)

First, install the base requirements (for building dwm and flex)

```bash
sudo apt install make gcc build-essential xorg libxinerama-dev libx11-dev libxft-dev suckless-tools brightnessctl pulseaudio feh picom cargo libxkbcommon-dev
sudo apt install cava tty-clock neofetch scrot # optional
cargo install alacritty
```

You also have to install libxft-bgra (here are the commands for debian):

```bash
# Install dependencies
sudo apt install libfreetype-dev fontconfig libxext-dev libxrender-dev xutils-dev dh-autoreconf git make

# Download and patch repository
git clone https://gitlab.freedesktop.org/xorg/lib/libxft.git
wget https://gitlab.freedesktop.org/xorg/lib/libxft/merge_requests/1.patch
cd libxft/
patch -p1 < ../1.patch

# Compile and install
sh autogen.sh --sysconfdir=/etc --prefix=/usr --mandir=/usr/share/man
make
sudo make install

# Replace old libxft by the new patched version
mv /usr/lib/libXft* /usr/lib/x86_64*
```

## Installation
> **WARNING**, you WILL have to modify the commands in .xinitrc file. Some of those depennds on your hardware. Also note you can replace "cp" commands by "ln -s" commands to kepp all the files in one place.

To install all dotfiles, make sure to remove your existent config

```bash
git clone https://codeberg.org/SnowCode/rice
cd rice/
stow */
```

To install dwm do this:

```bash
cd ~/dwm
make
sudo make clean install
```
