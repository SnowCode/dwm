PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '

# Env variables
export TERM=linux
export EDITOR=nano

# Using underscore as text cursor
#echo -ne "\x1b[3 q" #echo -ne "\x1b[6 q"
#cat ~/.cache/wal/sequences

# Colorful stuff
alias grep='grep --color=auto'
alias ls='ls --color=auto'
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Other aliases
alias irc='ssh debian@51.178.55.122 -t tmux attach -t weechat'
alias vps='ssh debian@51.178.55.122'
alias quit='exit'
alias mpv='mpv --hwdec=vaapi'
alias qw='cd ~/gemini1/ && nano flight.gmi && ./reload.sh'


export LC_ALL=fr_BE.UTF-8
alias reload-other="ssh discord@51.178.55.122 -t ./anotherbot/reload.sh"
. "$HOME/.cargo/env"

export PATH=$PATH:~/.local/bin

alias switch="alacritty-colorscheme apply \$(alacritty-colorscheme list | fzf)"

